from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.task_form,  name='task_insert'),
    path('<int:id>/', views.task_form, name='task_update'),
    path('list/', views.tasks_list,  name='tasks_list'),
    path('delete/<int:id>/', views.task_delete, name='task_delete'),
]
