from django.db import models

# Create your models here.


class TaskType(models.Model):
    title = models.CharField(max_length=64)

    def __str__(self):
        return self.title


class Task(models.Model):
    title = models.CharField(max_length=64, blank=False)
    done = models.BooleanField(default=False)
    created = models.DateField(auto_now_add=True)
    task_type = models.ForeignKey(TaskType, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
