from django.shortcuts import render, redirect
from .forms import TaskForm
from .models import Task


def tasks_list(req):
    context = {'task_list': Task.objects.all()}
    return render(req, 'todo_app/tasks_list.html', context)


# Если id=-1, то мы перешли на страницу с пустой формой для создания нового задания
def task_form(req, id=-1):
    if req.method == 'GET':
        if id == -1:
            form = TaskForm()
        else:
            task = Task.objects.get(pk=id)
            form = TaskForm(instance=task)

        return render(req, 'todo_app/task_form.html', {'form': form})
    else:
        if id == -1:
            form = TaskForm(req.POST)
        else:
            task = Task.objects.get(pk=id)
            form = TaskForm(req.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect('/list')


def task_delete(req, id):
    task = Task.objects.get(pk=id)
    task.delete()
    return redirect('/list')
